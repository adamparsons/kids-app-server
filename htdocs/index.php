<?php
/*
 * Index Page
 *  The default landing page for the "Kids App"
 *  Handles:
 *   - Loading Server Application
 *   - Manifest Caching
 */
error_reporting(E_ALL);
ini_set('display_errors', 1);

// Start a PHP session cookie
session_start();

// Load the application configuration file
require_once '../resources/config.KidsAppServer.php';

// HTML Settings
$AppName        = "Kids App";
$WebAppTitle    = "Kids App";
$WebAppManifest = "manifest.99.manifest";

?>

<!DOCTYPE html>

<html lang="en" manifest="<?=$WebAppManifest?>">
    <head>
        <!-- Page Title -->
        <title><?=$WebAppTitle?></title>

        <!-- Mobile / Web App meta tags -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="initial-scale=1, maximum-scale=1, minimal-ui">
        <meta name="mobile-web-app-capable" content="yes">
        <meta name="mobile-web-app-title" content="<?=$WebAppTitle?>">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-title" content="<?=$WebAppTitle?>">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">

        <!-- CSS / Bootstrap -->
        <? include "css/css.include.config" ?>

        <!-- jQuery / JS / Bootstrap-->
        <? include "js/js.include.config" ?>
    </head>

    <body><? handle_page(); ?></body>
</html>