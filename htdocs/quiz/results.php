﻿<!DOCTYPE html>
<html>
<head>
    <title>Results Screen</title>
    <meta charset="UTF-8">

    <link href="css/jquery.growl.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="css/result.css">

    <!-- App icon manifest -->
    <link rel="manifest" href="manifest.json">


    <? require_once '../../config/api.settings.php'; ?>
    <script src="js/jquery-1.11.3.min.js"></script>
    <script src="js/jquery.growl.js" type="text/javascript"></script>
    <script src="js/results_screen.js"></script>

    <!-- Mobile / Web App meta tags -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, minimal-ui">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-title" content="MCV Quiz">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-title" content="MCV Quiz">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">

</head>


<body> <!-- need to make this all responsive and fluid and stuff, because at the moment the canvas's stop half way down on a 1080p screen -->

    <div id="result1"></div>
    <div id="result2"></div>
    <div id="result3"></div>

    <div id="result19"></div>
    <div id="result20"></div>
    <div id="result21" >
    </div>

    
    <div id="text"> 
	<p> Most like you </p>
	<br>
	<br>
	<p> Least like you </p>
  </div>
  
  <div id="right" >
    <form method="post" onsubmit="return false;">
      <fieldset>
        <legend>How much is this like you?</legend> <!-- need to reword this probably, brain no workey -->
        <input type="radio" id="radio1" name="opinion" value="3" />A lot like me<br />
        <input type="radio" id="radio2" name="opinion" value="2" />A little like me<br />
        <input type="radio" id="radio3" name="opinion" value="1" />Not like me at all<br />
        <!-- If they don't leave a response maybe make it a default value of 0? -->
        <input type="submit" value="Submit" id="submit"/>
      </fieldset>
    </form>
  </div>
  


</body>


</html>
