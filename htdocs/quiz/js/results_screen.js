var RESULT_POST_URL = WebApp.host + "result_post";

function shuffle(o) {
    for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
    return o;
}

function testResults(numberToSend) {

        console.log("testResults : "+numberToSend);

        var child_id = typeof sessionStorage.child_id !== "undefined" ? sessionStorage.child_id : 0;
        var res_id   = typeof sessionStorage.researcher_email !== "undefined" ? sessionStorage.researcher_email : 0;

        results = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21];
        results = shuffle(results);

        rating = [1,2,2,3,3,3,3];
        rating = shuffle(rating);

        // Send request
        $.ajax(RESULT_POST_URL, {
            data: {
                security_code: child_id,
                researcher_email: res_id,
                rank_1:  results[0],
                rank_2:  results[1],
                rank_3:  results[2],
                rank_4:  results[3],
                rank_5:  results[4],
                rank_6:  results[5],
                rank_7:  results[6],
                rank_8:  results[7],
                rank_9:  results[8],
                rank_10: results[9],
                rank_11: results[10],
                rank_12: results[11],
                rank_13: results[12],
                rank_14: results[13],
                rank_15: results[14],
                rank_16: results[15],
                rank_17: results[16],
                rank_18: results[17],
                rank_19: results[18],
                rank_20: results[19],
                rank_21: results[20],
                reflection: rating[1]
            },
            success: function() {
                console.log('success');// send next one after 100 ms
                if (numberToSend > 1) {
                    setTimeout(function () { testResults(numberToSend - 1); }, 100);
                }
            },
            error: function(){
                console.error('error');
            }
        });
}

window.onload = function () {
    //var results = [-0.2, 0, 0.2, 0.4, 0.8, 0, -0.2, -0.2, 0.4000000000000001, -0.2, 0, -0.2, -0.4000000000000001, -0.2, 0.2, -0.4000000000000001, 0.4, -0.2, -0.6000000000000001, 0, 0.4];
    // ^ are some values I got from running through the test once, treating it as a real world example in case of testing leaving it here maybe?

    //make the line bellow a comment to make result page work on it's own - remove this up to and including line 2 when project is complete 
	//loads the results from the test from the sessionStorage
	var results;
		if(typeof sessionStorage.result === "undefined") {
			$.growl.warning({ message: "No results detected... (using default)" });
			results = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21];
            results = shuffle(results);
		} else
    		results = JSON.parse(sessionStorage.result);
    var resultObject = [{index:"", value:""}];//a list and value of each vid
	
	//sets the resultObject with data
    for(i = 0; i < results.length; i++){
	    resultObject[i] = {index: i , value: results[i]};
	};
    resultObject.sort(function (a, b) {
        return b.value - a.value//sorts it highest to lowest
    });

    var resultImages = [];//list of result images
    for(i = 0; i < results.length; i++){
	    resultImages[i] = resultObject[i].index + 1;//load each image index in 
    };
    
	//get the top and bottom 3 for viewing and displaying to the html
    var top = resultImages.slice(0,3);
    console.log(top);
    var bot = resultImages.slice(18,21);
    console.log(bot);

		var attempt = 0;

    var resultsToDraw = top.concat(bot);
    
//---------------------This bit bellow does the actual drawing----------------------\\


    //6 divs named exactly those numbers
    var divToDraw = ["1","2","3","19","20","21"]; //The numbers of the divs we are drawing into on the html, could've been just 1 to 6 but this makes it clearer what's happening
	//pushes html image elements into the html. 
    for (var i = 0; i < 6; i++){
        var y = resultsToDraw[i];
	    var img = i+1
	    var x = '<img id= "image' + img  +  '" src = "assets/thumbnails/thumbnail' + y + '.jpg">';
	    var thisDiv = "result" + divToDraw[i];
	    document.getElementById(thisDiv).innerHTML += x;
    };

	document.getElementById("submit").onclick = function() { resultSend(); };

	function getRating() {
		return $('input[name="opinion"]:checked').val();
	}
	
	function resultSend () {
		// Show "sending" notification
  		$.growl({ title: "Sending...", message: "Sending..." });

		var child_id = typeof sessionStorage.child_id !== "undefined" ? sessionStorage.child_id : 0;
		var res_id   = typeof sessionStorage.researcher_email !== "undefined" ? sessionStorage.researcher_email : 0;

		// Send request
		$.ajax(RESULT_POST_URL, {
			data: {
				security_code: child_id,
				researcher_email: res_id,
				rank_1:  results[0],
				rank_2:  results[1],
				rank_3:  results[2],
				rank_4:  results[3],
				rank_5:  results[4],
				rank_6:  results[5],
				rank_7:  results[6],
				rank_8:  results[7],
				rank_9:  results[8],
				rank_10: results[9],
				rank_11: results[10],
				rank_12: results[11],
				rank_13: results[12],
				rank_14: results[13],
				rank_15: results[14],
				rank_16: results[15],
				rank_17: results[16],
				rank_18: results[17],
				rank_19: results[18],
				rank_20: results[19],
				rank_21: results[20],
				reflection: getRating()
			},
			success: resultOnSuccess,
			error: resultOnError
		});
	}

	var resultOnSuccess = function(data) {
  		$.growl.notice({ title: "Success", message: "Your result was sent! (Record: " + data.Success + ")"});
  		result_has_been_sent = true;
  		setTimeout(goToSuccessPage, 1000);
	};

	var resultOnError = function(xhr, status, errorType) {
		data = xhr.responseJSON;

		if(typeof data !== "undefined" && typeof data.Error !== "undefined") {
			// Error returned from Server
  			$.growl.warning({ message: "Error: "+data.Error + " Retrying (Attempt "+(attempt+1)+" out of 3)" });

		} else {
			// Error connecting to Server
  			$.growl.warning({ message: "Error: Server rejected request (Attempt "+(attempt+1)+" out of 3)" });
		}

		attempt += 1;

		if(attempt < 3) {
			// Retry
			setTimeout(resultSend,1500);
		} else {
  			$.growl.error({ message: "Could not send message." });
			attempt = 0;
		}
	};

	var goToSuccessPage = function() {
  		$.growl.notice({ title: "Redirecting", message: ""});
		window.location.replace("playagain.html");
	};
};
