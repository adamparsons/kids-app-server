/*
 *  Web App Video Caching
 *  Author: Reece Como
 */

// Cache Status Values
var cacheStatusValues = [
    'uncached',
    'idle',
    'checking',
    'downloading',
    'updateready',
    'obsolete'
];

// Attach logging to the Cache events
var cache = window.applicationCache;
cache.addEventListener('cached', logEvent, false);
cache.addEventListener('checking', logEvent, false);
cache.addEventListener('downloading', logEvent, false);
cache.addEventListener('error', logEvent, false);
cache.addEventListener('noupdate', logEvent, false);
cache.addEventListener('obsolete', logEvent, false);
cache.addEventListener('progress', logEvent, false);
cache.addEventListener('updateready', logEvent, false);

function logEvent(e) {
    var online, status, type, message;
    online = navigator.onLine ? 'yes' : 'no';
    status = cacheStatusValues[cache.status];
    type = e.type;
    message = 'online: ' + online;
    message+= ', event: ' + type;
    message+= ', status: ' + status;
    if (type == 'error' && navigator.onLine) {
        message += ' (prolly a syntax error in manifest)';
    }
    console.log(message);
    if(navigator.onLine) {
        if(type == "cached")
            alert("Saved!");
        else
        if(type == "noupdate" && status == "idle")
            alert("loaded!");
    } else {
        if(type == "error" && status == "idle")
            alert("loaded!");
    }
}

window.applicationCache.addEventListener(
    'updateready',
    function(){
        window.applicationCache.swapCache();
        console.log('swap cache has been called');
    },
    false
);

setInterval(function(){cache.update()}, 10000);

if (window.navigator.standalone) {
    // fullscreen mode
    alert("Webapp opened");
}