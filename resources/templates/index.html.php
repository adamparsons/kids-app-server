<div class="container">

    <? include "part.navbar.php"; ?>

    <div class="row">
        <!-- Results dashboard -->
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="filler-img" style="background-image:url('/img/ui_results.png');"></div>
                <div class="caption">
                    <h3>Results Dashboard</h3>
                    <p>Access, view, analyse, download and export anonymous user responses.</p>
                    <p><a href="/?page=results" class="btn btn-primary" role="button"><?=icon('list-alt')?>&nbsp; View Results</a></p>
                </div>
            </div>
        </div>

        <!-- Results dashboard -->
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="filler-img" style="background-image:url('/img/ui_videos.png');"></div>
                <div class="caption">
                    <h3>Researchers Dashboard</h3>
                    <p>Add and remove researchers and provisioning student PINs.</p>
                    <p><a href="/?page=researchers" class="btn btn-primary" role="button"><?=icon('list-alt')?>&nbsp; View Researchers</a>
                        <a href="#" class="btn btn-success" role="button"><?=icon('plus')?></a></p>
                </div>
            </div>
        </div>

        <!-- Results dashboard -->
        <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="filler-img" style="background-image:url('/img/ui_researchers.png');"></div>
                <div class="caption">
                    <h3>API Settings / Help</h3>
                    <p>View webhook information and API access points.</p>
                    <p><a href="/?page=apihelp" class="btn btn-default" role="button"><?=icon('list-alt')?>&nbsp; Settings</a>
                </div>
            </div>
        </div>

    </div>

    <? include "part.footer.php" ?>
</div>