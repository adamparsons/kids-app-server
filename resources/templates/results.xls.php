<?php
date_default_timezone_set('Australia/Perth');
$date = date('m-d-Y-h-i-s-a', time());

$filename = 'kidsvalues_results_'.$date.'.xls';

if(!isset($_GET['preview'])) {
    header('Content-type: application/excel');
    header('Content-Disposition: attachment; filename=' . $filename);
}

$data = '';
$researchers = [];

/*
 * Turn Results into a table
 */
foreach(Result::getAll('1=1', 'id', PHP_INT_MAX) as $result) {

    if(empty($result->researcher_id))
        $result->researcher = "None";
    else {
        // Index researchers by id
        if (array_key_exists($result->researcher_id, $researchers)) {
            // Load cached researcher
            $result->researcher = $researchers[$result->researcher_id];
        } else {
            if (($researcher = Researcher::find($result->researcher_id)) !== false) {
                $researchers[$researcher->id] = $researcher->name;
            }
        }
    }

    // Student Number
    $result->security = empty($result->security_code) ? "None" : $result->security_code;

    // RESULT FORMAT:
    // ID | RESEARCHER EMAIL | SECURITY CODE | RESULTS 1 --> 21 | REFLECTION | SUBMISSION DATE
    $data .= "<tr><td>$result->id</td><td>$result->researcher</td><td>$result->security</td>";
    for($i =1; $i <= 21; $i++) {
        $data .= '<td>'.$result->{'rank_'.$i}.'</td>';
    }
    $data .= "<td>$result->reflection</td><td>$result->created_on</td></tr>";
}

?>
<html xmlns:x="urn:schemas-microsoft-com:office:excel">
    <head>
        <!--[if gte mso 9]>
        <xml>
            <x:ExcelWorkbook>
                <x:ExcelWorksheets>
                    <x:ExcelWorksheet>
                        <x:Name>Sheet 1</x:Name>
                        <x:WorksheetOptions>
                            <x:Print>
                                <x:ValidPrinterInfo/>
                            </x:Print>
                        </x:WorksheetOptions>
                    </x:ExcelWorksheet>
                </x:ExcelWorksheets>
            </x:ExcelWorkbook>
        </xml>
        <![endif]-->
    </head>

    <body>
        <table>
            <tr><th>Database Entry ID</th><th>Researcher</th><th>Student Code</th><th colspan="21">Video Preferences 1 - 21</th><th>Reflection Accuracy 1 - 3</th><th>Submitted On</th></tr>
            <? print $data; ?>
        </table>
    </body>
</html>
