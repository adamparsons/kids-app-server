<div class="navbar navbar-default">
    <div class="container-fluid">
        <ul class="nav navbar-nav navbar-left">
            <li class="navbar-brand">
                <a href="/"><?=icon('home')?> &nbsp;<?=$AppName?> </a><span class="tag">- Dashboard</span>
            </li>
        </ul>

        <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?=icon('user')?>&nbsp; <? Authentication::$user; ?> <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="/?page=results"><?=icon('home')?>&nbsp; Dashboard</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="/?page=results"><?=icon('chevron-right')?>&nbsp; Results</a></li>
                    <li><a href="/?page=researchers"><?=icon('chevron-right')?>&nbsp; Researchers</a></li>
                    <li><a href="/?page=apihelp"><?=icon('chevron-right')?>&nbsp; API Settings</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="/?page=logout">Log Out</a></li>
                </ul>
            </li>
        </ul>

    </div>
</div>