<?php
	/*
	 *	Database Object
	 *		objects from database use this
	 *		as a template for ORM
	 *
	 *  Author: Reece Como
	 *
	 */

	interface MappedObject
	{
		public function load( $id );
		public function delete();
		public function save();
	}

	abstract class ActiveRecord extends \HubrORM\DatabaseConnection implements MappedObject, JsonSerializable
	{
		static 	$TABLE 			= null;
        static  $UNIQUE_FIELD   = null; // Alternate field for id lookup

		const 	SOFT_DELETE_ON	= false;
		const	SOFT_DELETE_COL = "is_deleted";

		static 	$FIELDS 		= [];	// List: "name", "dob"
		static 	$FIELDS_INPUT 	= [];	// K->V: "name" => "default", "dob" => "date"
		static 	$FIELDS_DEFAULT = ["id", "created_on", "last_modified"];
        static  $FIELDS_NO_JSON = [];

		static 	$FOREIGN_KEYS	= [];

		/*
		 *	Abstract methods
		 */
		//abstract protected function validate();
		//abstract protected

		/*
		 *	Constructor
		 */
		final public function __construct($id = null, $unique_field = "id")
		{
			parent::__construct(); // extend from DatabaseConnection

			if(empty($id))
                $this->init(); // create an empty obj
            else
            {
				if(is_array($id))
					$this->map($id); // map obj from a list of key-values
				else 
				{
                    if(!is_numeric($id) && $unique_field == "id" && !empty(static::$UNIQUE_FIELD))
                        $unique_field = static::$UNIQUE_FIELD;

                    // Create an object from database entry
					if(!$this->load($id, $unique_field)) {
						throw new Exception("No ".lcfirst(get_called_class())." found!");
					}
				}
			}
		}

        public function jsonSerialize() {
            $fields = array("id" => $this->id);
            foreach(static::$FIELDS as $field) {
                if(!in_array($field,static::$FIELDS_NO_JSON)) {
                    if (isset($this->$field))
                        if (is_object($this->$field))
                            $fields[$field] = $this->$field->id;
                        else
                            $fields[$field] = $this->$field;
                }
            }

            return $fields;
        }

		public static function get_fields() {
			return array_merge(static::$FIELDS_DEFAULT, static::$FIELDS);
		}
		public static function get_insert_fields() {
			return array_merge(static::$FIELDS_INSERT, 	static::$FIELDS);
		}

		/*
		 *	Load the object
		 *		by some unique identifier
		 *		(defaults to ID)
		 */
		public function load($id, $unique_field = "id")
		{
			// Assert that query wont fail
			if(empty($id)||empty(static::$TABLE))
				return false;

			$FIELDS = implode(",", static::get_fields());  // convert fields to format

            $soft_delete = "";
            if(static::SOFT_DELETE_ON)
                $soft_delete = "AND ".static::SOFT_DELETE_COL."=0";

			$data = Array(':id' => $id);			// clean ID
			$stmt = self::$database->prepare("SELECT ".$FIELDS." FROM ".static::$TABLE." WHERE $unique_field = :id $soft_delete");
			$stmt->execute($data);
			$row = $stmt->fetch(PDO::FETCH_ASSOC);

			if($row !== false)
				$this->map($row);

			return $row !== false;
		}

		/*
		 *	 Initiate an empty object
		 */
		protected function init()
		{
			// Assign every property to null
			foreach (static::get_fields() as $field)
				$this->$field = null;
		}

		/*
		 *	Map an array of properties
		 *	to the object
		 */
		protected function map($data) {
			foreach ($data as $property => $value)
			{
				$property = $this->mapForeignKey($property, $value);
				/* 
				 *  Store a copy of the property
				 *	as '_property' to allow you to
				 *	check which properties were changed
				 */
			    $this->{ "$property"}  = $value; // $id;	
			    $this->{"_$property"}  = $value; // $_id;
			}
		}

		protected function mapForeignKey($property,$value) {
			/*
			 *	Map foreign keys
			 */
			if($this->isForeignKey($property))
			{
				$class = static::$FOREIGN_KEYS[$property];
				try {
			    	$this->{ "$property" } = new $class($value);	// $this->event = new event($value);
			    } catch(Exception $NotFound) {
			    	$this->{ "$property" } = new $class();	// $this->event = new event();
			    }

			    return $property."_id";					// $this->event = $this->event_id;
			}

			return $property;
		}

        protected function isForeignKey($property) {
            return array_key_exists($property, static::$FOREIGN_KEYS);
        }

        protected function foreignKeyChanged($fk) {
            return is_object($fk) && $fk->id != $this->{"_".$field."_id"};
        }

		/*
		 *	Remove an object from
		 *		the database
		 */
		public function delete($force_hard = false) {
			if(static::SOFT_DELETE_ON && !$force_hard)
			{
				// "Delete" the object (in a reversible way)
				return $this->soft_delete();
			}
			else
			{
				// "Delete" the object (in an irreversible way)
				return $this->hard_delete();
			}
		}

		protected function soft_delete() {
            if(!static::SOFT_DELETE_ON)
                return false;

			$TABLE = static::$TABLE;
			$SOFT_DELETE_COL = static::SOFT_DELETE_COL;
			$update = self::$database->prepare("UPDATE $TABLE SET `$SOFT_DELETE_COL` = 1 WHERE id = $this->id");
			$update->execute();

			return $update->rowCount() == 1;
		}

		protected function hard_delete() {
			$TABLE = static::$TABLE;
			$update = self::$database->prepare("DELETE FROM $TABLE WHERE id = $this->id");
			$update->execute();

			return $update->rowCount() == 1;
		}

		/*
		 *	 Save changes to an object
		 *		into the database
		 */
		public function save() {
			// Check for validation issues
			if(!$err = $this->validate())
				return $err;

			if(!isset($this->id) || empty($this->id))
			{
			    // First time, create a new record
                return $this->insert();
			}
			else
			{
				// Update existing record
				return $this->update();
			}
		}

		protected function validate() { return true; }

		// Insert the values as a row in database
		public function insert() {
			$vals_tmp = Array();
			$inst_tmp = Array();

			foreach(static::get_fields() as $field)
				if(isset($this->$field) && $this->$field != null) {
					$inst_tmp[] = $field;
					if(is_object($this->$field))
						$vals_tmp[] = $this->$field->id;	// for foreign keys
					else
						$vals_tmp[] = $this->$field;		// for fields
				}

			$columns 	= implode(",", 	 $inst_tmp);
			$values 	= "?";
            for($i=1; $i < count($vals_tmp); $i++)
                $values .= ",?";

			$TABLE 		= static::$TABLE;

			$query = "INSERT INTO $TABLE ($columns) VALUES ($values);";
			$insert = self::$database->prepare($query);

			$insert->execute($vals_tmp);

			if($insert->rowCount() == 1) {
                $this->id = self::$database->lastInsertId();
                return $this->id;
            }

			return false;
		}

        // Convert [user=>1, group=>2] to "user='1' AND group='2'"
        protected static function arr_to_str($array) {
            $where_string = Array();
            foreach($array as $property => $value) {
                $where_string[] = "$property='".$value."'";
            }
            return implode(" AND ", $where_string);
        }

		// Update an existing row in the database
		protected function update() {

			$changed_fields = Array();	// Array of fields changed
			$changes_string = "";	// Array of field='value' strings

			// For each field
			foreach(static::$FIELDS as $field)
			{
				if($this->propertyIsChanged($field))
                {
                    $changed_fields[] = $this->$field;
                    $changes_string .= "$field = ?,";
				}
			}

			// No changes? Skip SQL
			if(count($changed_fields) == 0)
				return true;

			// Convert the changes into a string
			//	to insert into the database
			$TABLE = static::$TABLE;
			$changes = implode(",", $changes_string);
			$update = self::$database->prepare("UPDATE $TABLE SET $changes `last_modified` = CURRENT_TIMESTAMP WHERE id = $this->id");
			$update->execute($changed_fields);

			// If successful,
			//	update the _old variables too
			if($update->rowCount() == 1)
			{
				//foreach($changed_fields as $field)
				//	$this->{'_'.$field} = $this->$field;
				
				return true;
			}

			return false;
		}

        protected function propertyIsChanged($field) {
            $changed = isset($this->{"_$field"}) && $this->$field != $this->{'_'.$field};
            $fk_changed = $this->isForeignKey($field) && $this->foreignKeyChanged($field);
            return $fk_changed || $changed;
        }

		/*
		 *	 Save changes to an object
		 *		into the database
		 */
		public static function getAll($where = "1=1", $order_by = "id", $limit = 100)
		{
			global $database;

            if(is_array($where))
                $where = self::arr_to_str($where);

			$TABLE = static::$TABLE;
			$FIELDS = implode("`,`", static::get_fields());
			if(static::SOFT_DELETE_ON) {
				$SOFT_DEL = static::SOFT_DELETE_COL;
				$where .= " AND $SOFT_DEL = 0 ";
			}
			$select_all = $database->prepare("SELECT `$FIELDS` FROM `$TABLE` WHERE $where ORDER BY $order_by LIMIT 0, $limit");
			$select_all->execute();
			$query_result = $select_all->fetchall(PDO::FETCH_ASSOC);

			$objects = Array();

			foreach($query_result as $obj)
				$objects[] = new Static($obj);


			return $objects;
		}

        /*
         *	 Save changes to an object
         *		into the database
         */
        public static function get($params)
        {
            global $database;

            $where = [];
            $order_by = 'id';
            $start = 0;
            $limit = 0;

            extract($params, EXTR_OVERWRITE);

            if(!empty($where))
                $where = self::arr_to_str($where);
            else
                $where = "1=1";

            $TABLE = static::$TABLE;
            $FIELDS = implode("`,`", static::get_fields());
            if(static::SOFT_DELETE_ON) {
                $SOFT_DEL = static::SOFT_DELETE_COL;
                $where .= " AND $SOFT_DEL = 0 ";
            }
            $select_all = $database->prepare("SELECT `$FIELDS` FROM `$TABLE` WHERE $where ORDER BY $order_by LIMIT $start,$limit");
            $select_all->execute();
            $query_result = $select_all->fetchall(PDO::FETCH_ASSOC);

            $objects = Array();

            foreach($query_result as $obj)
                $objects[] = new Static($obj);


            return $objects;
        }

        public static function getLast($number, $where = "1=1", $order_by = "id") {
            global $database;

            if(is_array($where))
                $where = self::arr_to_str($where);

            $TABLE = static:: $TABLE;
            $FIELDS = implode("`,`", static::get_fields());
            if(static::SOFT_DELETE_ON) {
                $SOFT_DEL = static::SOFT_DELETE_COL;
                $where .= " AND $SOFT_DEL = 0 ";
            }

            $query = 'SELECT * FROM (SELECT `'.$FIELDS.'` FROM `'.$TABLE.'` WHERE '.$where.' ORDER BY '.$order_by.' DESC LIMIT 0, '.$number.') t ORDER BY id ASC';

            $select_all = $database->prepare($query);
            $select_all->execute();

            $query_result = $select_all->fetchall(PDO::FETCH_ASSOC);

            $objects = Array();

            foreach($query_result as $obj)
                $objects[] = new Static($obj);


            return $objects;
        }

		public static function count($where = "1=1") {
			global $database;
			$select_all = $database->prepare("SELECT COUNT(*) as count FROM ".static::$TABLE." WHERE $where");
			$select_all->execute();
			return $select_all->fetch()[0];
		}

		/*
		 *	Get an object from
		 *		some parameters
		 */
		public static function find($where, $order_by = "id") {
			global $database;

			if(is_array($where))
                $where = self::arr_to_str($where);

			$TABLE = static::$TABLE;
			$FIELDS = implode(",", static::get_fields());
			if(static::SOFT_DELETE_ON) {
				$SOFT_DEL = static::SOFT_DELETE_COL;
				$where .= " AND $SOFT_DEL = 0 ";
			}
			$select_all = $database->prepare("SELECT $FIELDS FROM $TABLE WHERE $where ORDER BY $order_by");

			$select_all->execute();

			if($row = $select_all->fetch(PDO::FETCH_ASSOC)) {
				return new Static($row);
			}

			return false;
		}

		/*
		 *
		 */
		public static function create() {

		}

	}
?>