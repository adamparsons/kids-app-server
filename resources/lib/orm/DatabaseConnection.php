<?php
/**
 *	Database Connection Class
 *      Designed for MySQL, but also portable to other
 *      systems.
 *
 *  Author: Reece Como
 *
 */

namespace HubrORM;
use PDO;

class DatabaseConnection
{
	static $database;

	public function __construct()
	{
		// Create the connection
        //  as a static singleton (that further connections will simply reuse)
        if(empty(self::$database))
            try {
                self::$database = new PDO("mysql:host=" . DATABASE_HOST . ";dbname=" . DATABASE_NAME, DATABASE_USERNAME, DATABASE_PASSWORD);
                self::$database->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                self::$database->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
            } catch(\PDOException $e) {
                throw new \Exception("There was an error connecting to the database server.");
            }
	}

    public static function connect()
    {
        try {
            new self;
            return self::$database;
        } catch (\Exception $e) {
            return false;
        }
    }
}
?>