<?php
/*
 * Helper Methods
 * Author: Reece Como
 */

// Autoload classes
spl_autoload_register('load_class');

function request_is_ajax() { #return bool;
    return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
}

function logged_in() { #return bool;
    return Authentication::$logged_in; // Get logged in param
}

function icon($icon) { #return String;
    return "<span class='glyphicon glyphicon-$icon'></span>";
}

function load_class($class) {
    require "../resources/objects/$class.class.php";
}

function json_dump($var) {
    ob_clean();              // Clear the HTML before it
    exit(json_encode($var)); // Encode to JSON and end transmission
}

function json_error($string) {
    http_response_code(400);
    json_dump(["Error"=> $string]);
}

function set_error($myError) {
    global $error;
    $error = $myError;
}

function required_vars($array) {
    foreach($array as $var) {
        if(!isset($var))
            return false;
    }
    
    return true;
}

function set_ajax() {
    ob_clean(); // Clear any existing HTML buffer

    header("Access-Control-Allow-Origin: *"); // Allow XSS (CORS)
    header('Content-Type: application/json'); // Allow JSON
}

function handle_page() {
    global $valid_pages;

    // Assert valid page or index
    $page = isset($_GET['page']) && in_array($_GET['page'], $valid_pages) ? $_GET['page'] : "index";

    // Call the page controller
    $controller = "ctrl_".$page;
    $controller();
}

function display_template($template, $params = []) {
    // give templates access to global vars
    global $error, $AppName, $WebAppName;
    extract($params);
    include "../resources/templates/$template.html.php";
}

function download_file($template, $format = "xls", $params = []) {
    global $error, $AppName, $WebAppName;
    extract($params);
    include "../resources/templates/$template.$format.php";
}

function page_url($page, $params = "") {
    if(is_array($params)) {
        $new_params = "";
        foreach($params as $param => $value) {
            $new_params .= "$param=$value&";
        }
        $params = $new_params;
    }
    // Prepend a query string
    if(!empty($params))
        $params = "?".$params;

    // http://www.mydomain.com/login?yes=1
         return "/$page$params";

    // http://www.mydomain.com/?page=login&yes=1
    //return "/?page=$page&$params";
}

function set_page($page) {
    // http://www.mydomain.com/login
         header("Location: $page");

    // http://www.mydomain.com/?page=login
    //header("Location: ?page=$page");
}

