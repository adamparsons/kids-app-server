<?php

class Authentication {
    static $logged_in = false;
    static $user      = false;
    static $err       = "";

    const AUTH_STATE_VAR = "AUTH";

    /*
     * Loads logged in status to state
     */
    public function __construct()
    {
        if(self::session_is_set()) {
            if($email = self::get_session()) {
                self::$logged_in = true;     // set the global logged in state
                self::$user      = $email;   // set the global user
            }
        }
    }

    public static function login($email, $password) {
        try {
            $user = new User($email); // If user exists
            if (password_verify($password, $user->password)) // and password hash is verified
            {
                session_regenerate_id(true); // Regenerate the session id (prevent session impersonation)
                self::set_session($email);   // Set the session var to include email address

                return true; // Return to the calling logic
            } else {

                self::set_error("Username/Password could not be verified (2)");
                return false;
            }

        } catch(Exception $e) {
            // Otherwise incorrect
            self::set_error("Username/Password could not be verified ".$e->getMessage());
            return false; // Either user or password was wrong
        }
    }

    public static function logout() {
        unset($_SESSION[self::AUTH_STATE_VAR]);
        self::$logged_in = false;
        self::$user = null;
    }

    public static function set_error($err) {
        $_SESSION[self::AUTH_STATE_VAR."_ERR"] = $err;
    }

    public static function get_error() {
        if(!empty($_SESSION[self::AUTH_STATE_VAR."_ERR"]))
            return $_SESSION[self::AUTH_STATE_VAR."_ERR"];
        else
            return "";
    }

    /*
     * Session handlers
     */
    public static function set_session($value) {
        $_SESSION[self::AUTH_STATE_VAR] = $value;
    }
    public static function get_session() {
        return $_SESSION[self::AUTH_STATE_VAR];
    }
    public static function session_is_set() {
        return isset($_SESSION[self::AUTH_STATE_VAR]) && $_SESSION[self::AUTH_STATE_VAR] !== false;
    }

    public static function add_user($email, $password) {
        $user = new User();
        $user->email = $email;
        $user->password = self::crypt_hash($password);

        return $user->save();
    }

    public static function crypt_hash($input, $rounds = 7)
    {
        $crypt_options = array(
            'cost' => $rounds
        );
        return password_hash($input, PASSWORD_BCRYPT, $crypt_options);
    }
}