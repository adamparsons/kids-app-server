<?php

class Video extends ActiveRecord {

    static $TABLE = 'videos';

    static $FIELDS = [
        'name',
        'filename',
        'order',
        'date'
    ];
}