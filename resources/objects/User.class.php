<?php

class User extends ActiveRecord
{
    static $TABLE = "users";
    static $UNIQUE_FIELD = "email";
    static $SOFT_DELETE_ON = true;

    static $FIELDS = [
        'email',
        'password'
    ];

    static $FIELDS_NO_JSON = [
        'password'
    ];

}