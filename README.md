#Kids App Server
## Group K | Reece Como

### Setup Instructions
Pre-requisites: Server requires PHP 5.6.1 and MySQL 4.2 and enable PHPs Short Tags
1. Inside htdocs, create a directory (where php has write permissions) called `assets`
2. Add the file `/config/db.ini` to `git assume-unchanged` and then change the settings
